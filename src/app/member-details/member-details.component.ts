import { Component, OnInit, OnChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AppService } from '../app.service';
import { Router, ActivatedRoute } from '@angular/router';

// This interface may be useful in the times ahead...
interface Member {
  firstName: string;
  lastName: string;
  jobTitle: string;
  team: string;
  status: string;
}

@Component({
  selector: 'app-member-details',
  templateUrl: './member-details.component.html',
  styleUrls: ['./member-details.component.css']
})
export class MemberDetailsComponent implements OnInit, OnChanges {
  memberModel: Member;
  memberForm: FormGroup;
  submitted = false;
  alertType: String;
  alertMessage: String;
  teams = [];
  memberId: number;

  constructor(private fb: FormBuilder, private appService: AppService, private router: Router, private route: ActivatedRoute) {}

  ngOnInit() {

    this.memberForm = this.fb.group({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      jobTitle: new FormControl('', Validators.required),
      status: new FormControl('', Validators.required),
      team: new FormControl('', Validators.required),
    })

    this.route.params.subscribe( ({id}) => {

      this.memberId = id

      if (this.memberId) {
        this.appService.getMember(id).subscribe( res => {

          this.submitted = true

          this.memberForm = this.fb.group({
            firstName: new FormControl(res.firstName, Validators.required),
            lastName: new FormControl(res.lastName, Validators.required),
            jobTitle: new FormControl(res.jobTitle, Validators.required),
            status: new FormControl(res.status, Validators.required),
            team: new FormControl(res.team, Validators.required),
          })
        })
      }
    })
   
    this.appService.getTeams().subscribe(teams => (this.teams = teams));
  }

  ngOnChanges() { 
  }

  // TODO: Add member to members
  onSubmit(form: FormGroup) {
    this.memberModel = form.value;

    if(this.memberId) {
      this.appService.updateMember(this.memberId, form).subscribe(res => this.router.navigateByUrl('/members'))
      return 
    }
    
    this.appService.addMember(form).subscribe(res => this.router.navigateByUrl('/members'))
  }
}
