import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css']
})
export class MembersComponent implements OnInit {
  members = [];

  constructor(public appService: AppService, private router: Router) {}

  ngOnInit() {
    this.getMembers();
  }

  getMembers(){
    this.appService.getMembers().subscribe(members => (this.members = members));
  }

  goToAddMemberForm() {
    this.router.navigate(['/member_details']);
  }

  logout() { 
    this.appService.username = '';
    this.router.navigate(['/login']);
  }

  editMemberByID(id: number) {
    this.router.navigate(['/member_details', id]);
  }

  deleteMemberById(id: number) {
    this.appService.deleteMember(id).subscribe(res=>this.getMembers())
  }
}
